
Description
===========

This is a jinja2 based script use to generated slides for reveal.js.

Copying
=======

Copyright (C) 2013-2018, Aurélien Chabot <aurelien@chabot.fr>

Licensed under **MIT**.

See COPYING file.
