#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    reveal.js generator
    ~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2013-2018 by Aurélien Chabot <aurelien@chabot.fr>
    :license: MIT, see COPYING for more details.
"""
try:
    from distutils import dir_util
    from docutils import core
    import configparser
    import jinja2
    import json
    import traceback
    import sys
    import os
    import re
except ImportError as error:
    print('ImportError: ', str(error))
    exit(1)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
CONTENT_DIR = CURRENT_DIR

if len(sys.argv) > 1:
    CONTENT_DIR = str(sys.argv[1])

INPUT = os.path.join(CONTENT_DIR, 'content')
OUTPUT = os.path.join(CONTENT_DIR, 'out')
TEMPLATE_PATH = os.path.join(CONTENT_DIR, 'templates')
TEMPLATE_OPTIONS = {}

# Settings

try:
    config = configparser.RawConfigParser(allow_no_value=True)
    config.read(os.path.join(CONTENT_DIR, 'site.cfg'))

    SITE = {
        "title"            : config.get('SITE', 'title'),
        "title_reveal"     : config.get('SITE', 'title_reveal'),
        "author"           : config.get('SITE', 'author'),
        "author_reveal"    : config.get('SITE', 'author_reveal'),
        "description"      : config.get('SITE', 'description'),
        "theme"            : config.get('SITE', 'theme'),
        "syntax_highlight" : config.get('SITE', 'syntax_highlight'),
    }
except:
    print("Error while parsing configuration file : " +
            os.path.join(CONTENT_DIR, 'site.cfg'))
    traceback.print_exc()
    sys.exit()

# Stuff

STEPS = []

def step(func):
    def wrapper(*args, **kwargs):
        func(*args, **kwargs)
    STEPS.append(wrapper)
    return wrapper

def write_file(url, data):
    path = os.path.join(OUTPUT, url)
    dirs = os.path.dirname(path)
    if not os.path.isdir(dirs):
        os.makedirs(dirs)
    with open(path, 'w') as f:
        f.write(data)

def rst2html(s):
    overrides = {'input_encoding': 'unicode'}
    parts = core.publish_parts(source=s, writer_name='html', settings_overrides=overrides)
    parts['fragment'] = parts['fragment'].replace('<pre class="literal-block">', '<pre><code data-trim>')
    parts['fragment'] = parts['fragment'].replace('</pre>', '</code></pre>')
    return parts['html_title'] + "\n" + parts['fragment']

def md2html(s):
    return "<section data-markdown>" + s + "</section>"

def parse_file(path, name):

    print(" - ", name)

    with open(path, 'r') as f:
        try:
            content = ''
            rst = re.match(r'^[0-9].+\.(rst)$', name)

            if rst:
                content = '.. |fragment_in| raw:: html\n\n    <div class="fragment">\n\n'
                content += '.. |fragment_out| raw:: html\n\n    </div>\n\n'

            notes = ''
            notes_mark = False
            fragment_mark = False
            for line in f:
                if line.startswith("%"): # Beginning of notes section
                    notes_mark = True
                    continue
                elif line.startswith("@") and rst:
                    if fragment_mark:
                        content += '|fragment_out|\n'
                    content += '|fragment_in|\n'
                    fragment_mark = True
                    continue
                if notes_mark:
                    notes += line
                else:
                    content += line

            if fragment_mark:
                content += '|fragment_out|\n'

            if re.match(r'^[0-9].+\.(rst)$', name):
                # Convert rest to html
                content = rst2html(content)
            elif re.match(r'^[0-9].+\.(md)$', name):
                content = md2html(content)
            elif not re.match(r'^[0-9].+\.(html)$', name):
                print("Incompatible file type extension")
                return None, None

            return content, notes
        except:
            print("Unexpected error:", sys.exc_info()[0])
            traceback.print_exc()

    return None, None

def get_name(path, folder):

    try:
        with open(os.path.join(path, "name"), 'rU') as f:
            return f.readline().strip()
    except IOError:
        print("[WARNING] No name file, use folder name")
        return folder

def get_tree(source):

    files = []
    sections = []

    for f in sorted(os.listdir(source)):

        p = os.path.join(source, f)
        if not os.path.isdir(p):
            content, notes = parse_file(p, f)
            if content != None:
                files.append({'section': None, 'content': content, 'notes': notes})
        else:
            # Section
            section_name = get_name(p, f)
            sections.append({'name': section_name})

            for f2 in sorted(os.listdir(p)):
                p2 = os.path.join(p, f2)
                if not os.path.isdir(p2):
                    content, notes = parse_file(p2, f2)
                    if content != None:
                        files.append({'section': section_name,
                                      'content': content,
                                      'notes': notes})

    return sections, files

def generate(jinja_env, sections, slides, env, name):
    print('  %s%s -> %s%s' % (TEMPLATE_PATH, name, OUTPUT, name))
    template = jinja_env.get_template(name)
    write_file(name, template.render({'sections': sections,
                                      'slides': slides,
                                      'site': env}))

@step
def build_tree(jinja_env):
    print('* Copying deps...')
    # Copy reveal.js
    dir_util.copy_tree(os.path.join(CURRENT_DIR, "reveal.js"), OUTPUT)
    # Copy base tree
    dir_util.copy_tree(os.path.join(CURRENT_DIR, "tree"), OUTPUT)

@step
def step_index(jinja_env):
    print('* Generating HTML...')
    sections, slides = get_tree(INPUT)
    generate(jinja_env, sections, slides, SITE, "index.html")

if __name__ == '__main__':
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_PATH), **TEMPLATE_OPTIONS)
    env.filters['escapejs'] = lambda v: json.dumps(v)
    for step in STEPS:
        step(env)
